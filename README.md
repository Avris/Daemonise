# Daemonise

Use this library to run any process in the background without blocking the console. It will start a child process, detach it, and save its PID to `daemonise.json` in order to be able to stop it later.  

## Installation

    pnpm add -D avris-daemonise
    
## Usage

The following command:

    node_modules/.bin/avris-daemonise start webserver node_modules/.bin/webpack-dev-server \\--config ./webpack.config.js
    
will execute `node_modules/.bin/webpack-dev-server --config ./webpack.config.js` in the background and save the PID to `./daemonise.json` under the name `webserver`.

Note that the dashes have to be escaped with a double backslash (`\\--option`), otherwise they are treated as options for `daemonise` and not for the child process.

`stdout` and `stderr` will be saved to `./daemonise.log`. You can change it with the `--log` option.

To stop the server, execute:

    node_modules/.bin/avris-daemonise stop webserver

Example of a Makefile for Nuxt with pnpm:

```
start:
	pnpm exec avris-daemonise start webserver pnpm run dev

stop:
	pnpm exec avris-daemonise stop webserver
```
    
Suggested `.gitignore` entries:

    daemonise.json
    daemonise.log

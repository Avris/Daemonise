#!/usr/bin/env node

'use strict';

const spawn = require('child_process').spawn,
    fs = require('fs');

const readDeamons = function() {
    try {
        return JSON.parse(fs.readFileSync('./daemonise.json').toString());
    } catch (ex) {
        return {}
    }
};

const writeDeamons = function(deamons) {
    fs.writeFileSync('./daemonise.json', JSON.stringify(deamons, null, 2));
};

require('yargs')
    .version('0.0.1')
    .usage('Usage: $0 <cmd> [options]')
    .command('start <name> <command> [args..]', 'Start a process in the background', () => {}, (argv) => {
        const deamons = readDeamons();

        if (argv.name in deamons) {
            console.error(`Deamon ${argv.name} is already running...`);
            process.exit(1);
        }

        const args = argv.args.map((arg) => ('' + arg).replace(/\\/g, ''));

        console.info(`Starting deamon "${argv.name}": ${argv.command} "${args.join('" "')}"`);

        const out = fs.openSync(argv.log, 'a'),
            err = fs.openSync(argv.log, 'a');

        console.info(`Log: ${argv.log}`);

        const child = spawn(argv.command, args, {
            stdio: [ 'ignore', out, err ],
            detached: true
        });

        console.info(`PID: ${child.pid}`);

        deamons[argv.name] = child.pid;
        writeDeamons(deamons);

        child.unref();
    })
    .command('stop <name>', 'Stop a process in the background', () => {}, (argv) => {
        const deamons = readDeamons();

        if (!(argv.name in deamons)) {
            console.error(`Deamon ${argv.name} is not running...`);
            process.exit(1);
        }

        const pid = deamons[argv.name];

        console.info(`Stopping deamon "${argv.name}" (PID: ${pid})...`);

        try {
            process.kill(pid);
        } catch (ex) {
            console.log(`Deamon wasn't running anyway.`)
        }

        delete deamons[argv.name];
        writeDeamons(deamons);
    })
    .option('log', {'default': './daemonise.log'})
    .demandCommand()
    .recommendCommands()
    .strict()
    .argv
